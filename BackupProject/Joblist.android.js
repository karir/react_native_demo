'use strict';

import React, {
  AppRegistry,
  Component,
  StyleSheet,
  ListView,
  TouchableHighlight,
  Image,
  Text,
  View
} from 'react-native';

var DATA_DUMMY = [{
    posters: {
      logo: 'https://dad0jvgioe6jb.cloudfront.net/logos/92/10292/s44430273.jpg',
      iconMap: require("./imageTest/Marker-50.png"),
      iconJob: require("./imageTest/Job-50.png")
    },
    opportunity: 'Sales - Semarang',
    company: 'At Star Cosmos',
    position: 'Sales (Non-Technical)',
    place: 'Semarang'
  },{
    posters: {
      logo: 'https://dad0jvgioe6jb.cloudfront.net/logos/85/719185/bankmega1.jpg',
      iconMap: require("./imageTest/Marker-50.png"),
      iconJob: require("./imageTest/Job-50.png")
    },
    opportunity: 'Sales - Semarang',
    company: 'At Star Cosmos',
    position: 'Sales (Non-Technical)',
    place: 'Semarang'
  },{
    posters: {
      logo: 'https://dad0jvgioe6jb.cloudfront.net/logos/25/10325/s58858052.jpg',
      iconMap: require("./imageTest/Marker-50.png"),
      iconJob: require("./imageTest/Job-50.png")
    },
    opportunity: 'Sales - Semarang',
    company: 'At Star Cosmos',
    position: 'Sales (Non-Technical)',
    place: 'Semarang'
  },{
    posters: {
      logo: 'https://dad0jvgioe6jb.cloudfront.net/logos/86/709686/PONGS.png',
      iconMap: require("./imageTest/Marker-50.png"),
      iconJob: require("./imageTest/Job-50.png")
    },
    opportunity: 'Sales - Semarang',
    company: 'At Star Cosmos',
    position: 'Sales (Non-Technical)',
    place: 'Semarang'
  },{
    posters: {
      logo: 'https://dad0jvgioe6jb.cloudfront.net/logos/53/9553/s58982788.jpg',
      iconMap: require("./imageTest/Marker-50.png"),
      iconJob: require("./imageTest/Job-50.png")
    },
    opportunity: 'Sales - Semarang',
    company: 'At Star Cosmos',
    position: 'Sales (Non-Technical)',
    place: 'Semarang'
  },
];

class Joblist extends Component {
    constructor() {
      super();
      this.state = {
        dataSource: new ListView.DataSource({
          rowHasChanged: (r1, r2) => r1 !== r2
        }).cloneWithRows(DATA_DUMMY)
      };
    }

    renderDummy(dummy) {
      return (
        <View style={styles.container}>
          <View style={styles.leftContainer}>
            <Image
              style={styles.thumbnail}
              source={{uri: dummy.posters.logo}} />
          </View>
          <View style={styles.rightContainer}>
            <View style={styles.vwFirst}>
              <Text style={styles.txtOpportunity}>{dummy.opportunity}</Text>
            </View>
            <View style={styles.vwSecond}>
              <Text style={styles.txtCompany}>{dummy.company}</Text>
            </View>
            <View style={styles.other}>
              <View style={styles.viewIcon}>
                <Image
                  style={styles.icon}
                  source={dummy.posters.iconJob}
                  />
              </View>
              <View style={styles.viewTxtFirst}>
                <Text style={styles.txtOther}>{dummy.position}</Text>
              </View>
              <View style={styles.viewIcon}>
                <Image
                  style={styles.icon}
                  source={dummy.posters.iconMap}
                  />
              </View>
              <View style={styles.viewTxtSecond}>
                <Text style={styles.txtOther}>{dummy.place}</Text>
              </View>
            </View>
          </View>
        </View>
      )
    }

    render() {
      return(
        <ListView
          style={styles.listview}
          dataSource={this.state.dataSource}
          renderRow={this.renderDummy}
          renderSeparator={(sectionID, rowID) => <View key={`${sectionID}-${rowID}`} style={styles.separator} />}
        />
      );
    }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
  },
  leftContainer: {
    flex: 1,
    alignItems: 'center',
  },
  rightContainer: {
    flex: 2,
  },
  vwFirst: {
    marginBottom: 3
  },
  vwSecond: {
    marginBottom: 10
  },
  other: {
    flex: 1,
    flexDirection: 'row',
  },
  separator: {
    height: 1,
    backgroundColor: '#000080',
  },
  viewTxtFirst: {
    flex: 1
  },
  viewTxtSecond: {
    flex: 0.4
  },
  viewIcon: {
    marginRight: 5
  },
  thumbnail: {
    width: 60,
    height: 80,
    resizeMode: 'contain'
  },
  icon: {
    width: 13,
    height: 13,
  },
  txtOpportunity: {
    fontSize: 17,
    color: "#4169e1"
  },
  txtCompany: {
    fontSize: 12
  },
  txtOther: {
    fontSize: 10
  },
  listview: {
    paddingTop: 20,
    backgroundColor: "#F5FCFF"
  }
});

module.exports = Joblist;
