'use strict';

import Config from "react-native-config";

if(Config && Config.KARIR_APP_ENV === 'production'){
    var url = {
        API_URL: 'https://www.karir.com',
        version: 1,
    }
}else{
    var url = {
        API_URL: 'http://karir.dev:3000',
        version: 1,
    }
}


module.exports = url;

