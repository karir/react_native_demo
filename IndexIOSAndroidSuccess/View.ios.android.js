'use strict';

var React = require('react-native');
var {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback
} = React;

var styles = StyleSheet.create({
  box: {
    backgroundColor: '#527FE4',
    borderColor: '#000033',
    borderWidth: 1,
  },
  container: {
    flex: 1,
    marginTop: 40
  },
  space: {
    marginBottom: 15
  }
});

var ViewBorderStyleExample = React.createClass({
  getInitialState() {
    return {
      showBorder: true
    };
  },

  render() {
    return (
      <View style={styles.container}>
        <TouchableWithoutFeedback onPress={this._handlePress}>
          <View style={styles.space}>
            <View style={{
              borderWidth: 1,
              borderRadius: 5,
              borderStyle: this.state.showBorder ? 'dashed' : null,
              padding: 5
            }}>
              <Text style={{fontSize: 11}}>
                Dashed border style
              </Text>
            </View>
            <View style={{
              marginTop: 5,
              borderWidth: 1,
              borderRadius: 5,
              borderStyle: this.state.showBorder ? 'dotted' : null,
              padding: 5
            }}>
              <Text style={{fontSize: 11}}>
                Dotted border style
              </Text>
            </View>
          </View>
        </TouchableWithoutFeedback>

        <View style={styles.space}>
          <View style={{backgroundColor: '#527FE4', padding: 5}}>
            <Text style={{fontSize: 11}}>
              Blue background
            </Text>
          </View>
        </View>

        <View style={styles.space}>
          <View style={{borderColor: '#527FE4', borderWidth: 5, padding: 10}}>
            <Text style={{fontSize: 11}}>5px blue border</Text>
          </View>
        </View>

        <View style={[styles.box, {padding: 5}]}>
          <Text style={{fontSize: 11}}>5px padding</Text>
        </View>
        <View style={[styles.box, {margin: 5}]}>
          <Text style={{fontSize: 11}}>5px margin</Text>
        </View>
        <View style={[styles.box, {margin: 5, padding: 5, alignSelf: 'flex-start'}]}>
          <Text style={{fontSize: 11}}>
            5px margin and padding,
          </Text>
          <Text style={{fontSize: 11}}>
            widthAutonomous=true
          </Text>
        </View>

        <View style={{borderWidth: 0.5, borderRadius: 5, padding: 5}}>
          <Text style={{fontSize: 11}}>
            Too much use of `borderRadius` (especially large radii) on
            anything which is scrolling may result in dropped frames.
            Use sparingly.
          </Text>
        </View>

        <View style={{borderRadius: 10, borderWidth: 1, width: 20, height: 20}} />

        <View style={{flexDirection: 'row'}}>
          <View
            style={{
              width: 95,
              height: 10,
              marginRight: 10,
              marginBottom: 5,
              overflow: 'hidden',
              borderWidth: 0.5,
            }}>
            <View style={{width: 200, height: 20}}>
              <Text>Overflow hidden</Text>
            </View>
          </View>
          <View style={{width: 95, height: 10, marginBottom: 5, borderWidth: 0.5}}>
            <View style={{width: 200, height: 20}}>
              <Text>Overflow visible</Text>
            </View>
          </View>
        </View>

        <View>
          <View style={{opacity: 0}}><Text>Opacity 0</Text></View>
          <View style={{opacity: 0.1}}><Text>Opacity 0.1</Text></View>
          <View style={{opacity: 0.3}}><Text>Opacity 0.3</Text></View>
          <View style={{opacity: 0.5}}><Text>Opacity 0.5</Text></View>
          <View style={{opacity: 0.7}}><Text>Opacity 0.7</Text></View>
          <View style={{opacity: 0.9}}><Text>Opacity 0.9</Text></View>
          <View style={{opacity: 1}}><Text>Opacity 1</Text></View>
        </View>

      </View>
    );
  },

  _handlePress() {
    this.setState({showBorder: !this.state.showBorder});
  }
});
