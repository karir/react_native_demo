import React, {
  AppRegistry,
  Component,
  StyleSheet,
  TextInput,
  View
} from 'react-native';

class helloios extends Component {
  constructor(props){
    super(props);
    this.state = {
      text: ''
    }
  }

  render() {
    return (
      <View style={{ borderBottomColor: '#000000', borderBottomWidth: 1, marginTop: 40 }}>
        <TextInput
          style={{height: 40, borderColor: 'gray', borderWidth: 1}}
          onChangeText={(text) => this.setState({text})}
          value={this.state.text}
        />
      </View>
    );
  }
}
