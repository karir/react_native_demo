'use strict';

var React = require('react-native');
var {
  AppRegistry,
  AppState,
  StyleSheet,
  Text,
  View
} = React;

var AppStateSubscription = React.createClass({
  getInitialState() {
    return {
      appState: AppState.currentState,
      previousAppStates: [],
    };
  },
  componentDidMount: function() {
    AppState.addEventListener('change', this._handleAppStateChange);
  },
  componentWillUnmount: function() {
    AppState.removeEventListener('change', this._handleAppStateChange);
  },
  _handleAppStateChange: function(appState) {
    var previousAppStates = this.state.previousAppStates.slice();
    previousAppStates.push(this.state.appState);
    this.setState({
      appState,
      previousAppStates,
    });
  },
  render() {
    return (
      <View style={styles.container}>
        <Text>Prev AppState: {JSON.stringify(this.state.previousAppStates)}</Text>
        <Text>Show currentState: {this.state.appState}</Text>
      </View>
    );
  }
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
});
