'use strict';

var React = require('react-native');
var {
  Switch,
  Text,
  View
} = React;

var DisabledSwitchExample = React.createClass({
  render() {
    return (
      <View>
        <Switch
          disabled={true}
          style={{marginBottom: 10}}
          value={true} />
        <Switch
          disabled={true}
          value={false} />
      </View>
    );
  },
});

module.exports = DisabledSwitchExample;
