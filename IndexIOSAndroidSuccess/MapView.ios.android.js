'use strict';

var React = require('react-native');
var {
  AppRegistry,
  Component,
  Image,
  MapView,
  PropTypes,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} = React;

var MapViewExample = require('./MapViewExample');
var AnnotationExample  =require('./AnnotationExample');

class helloios extends Component {
  render() {
    return (
      <View style={styles.container}>
        <MapViewExample />

        <MapView
          style={styles.map}
          showsUserLocation={true}
          followUserLocation={true}
        />

        <AnnotationExample style={styles.map} annotation={{
          title: 'More Info...',
          rightCalloutView: (
            <TouchableOpacity
              onPress={() => {
                alert('You Are Here');
              }}>
              <Image
                style={{width:30, height:30}}
                source={require('./imageTest/uie_thumb_selected.png')}
              />
            </TouchableOpacity>
          ),
        }}/>

        <AnnotationExample style={styles.map} annotation={{
          title: 'More Info...',
          onFocus: () => {
            alert('Annotation gets focus');
          },
          onBlur: () => {
            alert('Annotation lost focus');
          }
        }}/>

        <AnnotationExample style={styles.map} annotation={{
          draggable: true,
          onDragStateChange: (event) => {
            console.log('Drag state: ' + event.state);
          },
        }}/>

        <AnnotationExample style={styles.map} annotation={{
          title: 'You Are Purple',
          tintColor: MapView.PinColors.PURPLE,
        }}/>

        <AnnotationExample style={styles.map} annotation={{
          title: 'Thumbs Up!',
          image: require('image!uie_thumb_big'),
        }}/>

        <AnnotationExample style={styles.map} annotation={{
          title: 'Thumbs Up!',
          view: <View style={{
            alignItems: 'center',
          }}>
            <Text style={{fontWeight: 'bold', color: '#f007'}}>
              Thumbs Up!
            </Text>
            <Image
              style={{width: 90, height: 65, resizeMode: 'cover'}}
              source={require('image!uie_thumb_big')}
            />
          </View>,
        }}/>
        
        <MapView
          style={styles.map}
          region={{
            latitude: 39.06,
            longitude: -95.22,
          }}
          overlays={[{
            coordinates:[
              {latitude: 32.47, longitude: -107.85},
              {latitude: 45.13, longitude: -94.48},
              {latitude: 39.27, longitude: -83.25},
              {latitude: 32.47, longitude: -107.85},
            ],
            strokeColor: '#f007',
            lineWidth: 3,
          }]}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF'
  },
  map: {
    height: 150,
    margin: 10,
    borderWidth: 1,
    borderColor: '#000000',
  },
});
