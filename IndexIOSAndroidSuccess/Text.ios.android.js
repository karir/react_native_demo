import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View
} from 'react-native';

class helloios extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={{fontWeight: 'bold'}}>
          I am bold Nested Txt
          <Text style={{color: 'red'}}>
            and red Nested Txt
          </Text>
        </Text>

        <Text>
          <Text>First part and </Text>
          <Text>second part</Text>
        </Text>

        <View>
          <Text>First part and </Text>
          <Text>second part</Text>
        </View>
      </View>
  );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
});
