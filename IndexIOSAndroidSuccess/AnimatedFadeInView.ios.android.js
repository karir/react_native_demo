'use strict';

import React, {
  Animated,
  Easing,
  StyleSheet,
  Text,
  View
} from 'react-native'

class FadeInView extends React.Component {
   constructor(props) {
     super(props);
     this.state = {
       fadeAnim: new Animated.Value(0), // init opacity 0
     };
   }
   componentDidMount() {
       this.setTimeout = setTimeout(() => {
         Animated.timing(
           this.state.fadeAnim,
           {toValue: 1}
         ).start();
       },3000
     )
   }
   render() {
     return (
       <Animated.View
         style={{opacity: this.state.fadeAnim,
         transform: [{
           translateY: this.state.fadeAnim.interpolate({
             inputRange: [0, 1],
             outputRange: [150, 0]
           }),
         }],
       }}>
         {this.props.children}
       </Animated.View>
     );
   }
 }

module.exports = FadeInView;
