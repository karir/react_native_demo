'use strict';

var React = require('react-native');
var {
  SegmentedControlIOS,
  Text,
  View,
  StyleSheet
} = React;

var BasicSegmentedControlExample = React.createClass({
  render() {
    return (
      <View>
        <View style={{marginBottom: 10}}>
          <SegmentedControlIOS values={['One', 'Two']} />
        </View>
        <View>
          <SegmentedControlIOS values={['One', 'Two', 'Three', 'Four', 'Five']} />
        </View>
      </View>
    );
  }
});

module.exports = BasicSegmentedControlExample;
