'use strict';

import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View
} from 'react-native';

var SliderExample = require('./SliderExample');

class SliderIOSExample extends Component {
  render() {
    return (
      <View>
        <SliderExample />
        <SliderExample
          minimumValue={-1}
          maximumValue={2}
        />
        <SliderExample step={0.25} />
        <SliderExample
          minimumTrackTintColor={'red'}
          maximumTrackTintColor={'green'}
        />
        <SliderExample thumbImage={require('./imageTest/uie_thumb_big.png')} />
        <SliderExample trackImage={require('./imageTest/slider.png')} />
        <SliderExample
          minimumTrackImage={require('./imageTest/slider-left.png')}
          maximumTrackImage={require('./imageTest/slider-right.png')}
        />
      </View>
    );
  }
}
