'use strict';

var React = require('react-native');
var {
  AppRegistry,
  Component,
  StyleSheet,
  View,
  Text,
  TouchableHighlight,
  AlertIOS,
} = React;


class helloios extends Component {
  state: any;
  customButtons: Array<Object>;

  constructor(props) {
    super(props);

    this.saveResponse = this.saveResponse.bind(this);
    
    this.customButtons = [{
      text: 'Custom OK',
      onPress: this.saveResponse
    }, {
      text: 'Custom Cancel',
      style: 'cancel',
    }];

    this.state = {
      promptValue: undefined,
    };
  }

  saveResponse(promptValue) {
    this.setState({ promptValue: JSON.stringify(promptValue) });
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableHighlight
          style={styles.wrapper}
          onPress={() => AlertIOS.prompt('Plain Text Entry')}>
          <View style={styles.button}>
            <Text>
              plain-text
            </Text>
          </View>
        </TouchableHighlight>

        <TouchableHighlight
          style={styles.wrapper}
          onPress={() => AlertIOS.prompt('Secure Text', null, null, 'secure-text')}>
          <View style={styles.button}>
            <Text>
              secure-text
            </Text>
          </View>
        </TouchableHighlight>

        <TouchableHighlight
          style={styles.wrapper}
          onPress={() => AlertIOS.prompt('Login & Password', null, null, 'login-password')}>

          <View style={styles.button}>
            <Text>
              login-password
            </Text>
          </View>
        </TouchableHighlight>

        <Text style={{marginBottom: 10}}>
          <Text style={{fontWeight: 'bold'}}>Prompt value:</Text> {this.state.promptValue}
        </Text>

        <TouchableHighlight
          style={styles.wrapper}
          onPress={() => AlertIOS.prompt('Type a value', null, this.saveResponse)}>

          <View style={styles.button}>
            <Text>
              prompt with title & callback
            </Text>
          </View>
        </TouchableHighlight>

        <TouchableHighlight
          style={styles.wrapper}
          onPress={() => AlertIOS.prompt('Type a value', null, this.customButtons)}>

          <View style={styles.button}>
            <Text>
              prompt with title & custom buttons
            </Text>
          </View>
        </TouchableHighlight>

        <TouchableHighlight
          style={styles.wrapper}
          onPress={() => AlertIOS.prompt('Type a value', null, this.saveResponse, undefined, 'Default value')}>

          <View style={styles.button}>
            <Text>
              prompt with title, callback & default value
            </Text>
          </View>
        </TouchableHighlight>

        <TouchableHighlight
          style={styles.wrapper}
          onPress={() => AlertIOS.prompt('Type a value', null, this.customButtons, 'login-password', 'admin@site.com')}>

          <View style={styles.button}>
            <Text>
              prompt with title, custom buttons, login/password & default value
            </Text>
          </View>
        </TouchableHighlight>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  wrapper: {
    marginTop: 10,
    borderRadius: 5,
    marginBottom: 5,
  },
  button: {
    backgroundColor: '#eeeeee',
    padding: 10,
  },
});
