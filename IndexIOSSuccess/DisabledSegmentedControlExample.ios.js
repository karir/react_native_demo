'use strict';

var React = require('react-native');
var {
  SegmentedControlIOS,
  Text,
  View,
  StyleSheet
} = React;

var DisabledSegmentedControlExample = React.createClass({
  render() {
    return (
      <View>
        <View>
          <SegmentedControlIOS enabled={false} values={['One', 'Two']} selectedIndex={1} />
        </View>
      </View>
    );
  },
});

module.exports = DisabledSegmentedControlExample;
