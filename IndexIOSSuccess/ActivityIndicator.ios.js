'use strict';

var React = require('react-native');
var {
  AppRegistry,
  ActivityIndicatorIOS,
  StyleSheet,
  View,
} = React;

var TimerMixin = require('react-timer-mixin');

var ToggleAnimatingActivityIndicator = React.createClass({
  mixins: [TimerMixin],

  getInitialState: function() {
    return {
      animating: true,
    };
  },

  setToggleTimeout: function() {
    this.setTimeout(
      () => {
        this.setState({animating: !this.state.animating});
        this.setToggleTimeout();
      },
      1200
    );
  },

  componentDidMount: function() {
    this.setToggleTimeout();
  },

  render: function() {
    return (
      <ActivityIndicatorIOS
        animating={this.state.animating}
        style={[styles.centering, {height: 80}]}
        size="large"
        hidesWhenStopped={false}
        color="#aa3300"
      />
    );
  }
});

var styles = StyleSheet.create({
  centering: {
    marginTop: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
