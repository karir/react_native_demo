'use strict';

var React = require('react-native');
var {
  SegmentedControlIOS,
  Text,
  View,
  StyleSheet
} = React;

var PreSelectedSegmentedControlExample = React.createClass({
  render() {
    return (
      <View>
        <View>
          <SegmentedControlIOS values={['One', 'Two']} selectedIndex={0} />
        </View>
      </View>
    );
  }
});

module.exports = PreSelectedSegmentedControlExample;
