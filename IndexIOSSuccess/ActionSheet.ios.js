'use strict'

var React = require('react-native');
var {
  ActionSheetIOS,
  StyleSheet,
  Text,
  UIManager,
  View,
  AppRegistry
} = React;

var BUTTONS = [
  'Option 0',
  'Option 1',
  'Option 2',
  'Delete',
  'Cancel',
];

var DESTRUCTIVE_INDEX = 3;
var CANCEL_INDEX = 4;

var ActionSheetExample = React.createClass({
  getInitialState() {
    return {
      clicked: 'test',
      textShare: '',
      textScreen: ''
    };
  },

  render() {
    return (
      <View>
        <View>
          <Text onPress={this.showActionSheet} style={style.button}>
            Click to show the ActionSheet
          </Text>
          <Text>
            Clicked button: {this.state.clicked}
          </Text>
        </View>
        <View>
          <Text onPress={this.showShareActionSheet} style={style.button}>
            Click to show the Share ActionSheet
          </Text>
          <Text>
            Test Share: {this.state.textShare}
          </Text>
        </View>
        <View>
          <Text onPress={this.showShareScreenshotSheet} style={style.button}>
            Click to show the Share ActionSheet
          </Text>
          <Text>
            Test Screenshot Share: {this.state.textScreen}
          </Text>
        </View>
      </View>
    );
  },

  showActionSheet() {
    ActionSheetIOS.showActionSheetWithOptions({
      options: BUTTONS,
      cancelButtonIndex: CANCEL_INDEX,
      destructiveButtonIndex: DESTRUCTIVE_INDEX,
    },
    (buttonIndex) => {
      this.setState({ clicked: BUTTONS[buttonIndex] });
    });
  },

  showShareActionSheet() {
    ActionSheetIOS.showShareActionSheetWithOptions({
      url: this.props.url,
      message: 'message to go with the shared url',
      subject: 'a subject to go in the email heading',
      excludedActivityTypes: [
        'com.apple.UIKit.activity.PostToTwitter'
      ]
    },
    (error) => alert(error),
    (success, method) => {
      var textShare;
      if (success) {
        textShare = `Shared via ${method}`;
      } else {
        textShare = 'You didn\'t share';
      }
      this.setState({textShare});
    });
  },

  showShareScreenshotSheet(){
    UIManager.takeSnapshot('window').then((uri) => {
      ActionSheetIOS.showShareActionSheetWithOptions({
        url: uri,
        excludedActivityTypes: [
          'com.apple.UIKit.activity.PostToTwitter'
        ]
      },
      (error) => alert(error),
      (success, method) => {
        var textScreen;
        if (success) {
          textScreen = `Shared via ${method}`;
        } else {
          textScreen = 'You didn\'t share';
        }
        this.setState({textScreen});
      });
    }).catch((error) => alert(error));
  }

});


var style = StyleSheet.create({
  button: {
    marginTop: 50,
    marginBottom: 10,
    fontWeight: '500',
  }
});
