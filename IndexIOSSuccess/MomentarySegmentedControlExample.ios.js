'use strict';

var React = require('react-native');
var {
  SegmentedControlIOS,
  Text,
  View,
  StyleSheet
} = React;

var MomentarySegmentedControlExample = React.createClass({
  render() {
    return (
      <View>
        <View>
          <SegmentedControlIOS values={['One', 'Two']} momentary={true} />
        </View>
      </View>
    );
  }
});

module.exports = MomentarySegmentedControlExample;
